import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
volts = np.array([1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8])
freq_sim = np.array([2.62E8, 5.05E8, 7.87E8, 1.1E9, 1.41E9, 1.73E9, 2.04E9, 2.34E9, 2.63E9])
freq_extract = np.array([1.43E8, 2.79E8, 4.42E8, 6.22E8, 8.13E8, 1.01E9, 1.19E9, 1.38E9, 1.56E9])
freq_ideal = []

volts_ = np.arange(1.0, 1.8, 0.01)
freq_ideal = []
for volt in volts_:
	value = min(2E9, max(0.5E9, volt*3.4E9-3.9E9))
	freq_ideal.append(value)

z= np.polyfit(volts, freq_extract, 2)
print(z)
p = np.poly1d(z)

model = p(volts)
print(max(abs(model-freq_extract)))

print(p(1))
p1 = np.polyder(p)
print(p1(1.5003))
fig, ax = plt.subplots()
ax.plot(volts, model)
ax.plot(volts, freq_extract)


ax.set(xlabel='Voltage', ylabel='Frequency',
       title='VCO Response')
ax.grid()

#fig.savefig("test.png")
plt.show()
