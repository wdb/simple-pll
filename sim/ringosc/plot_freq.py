import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
volts = np.arange(0.0, 1.8, 0.01)
freq = []
for volt in volts:
	value = min(2E9, max(0.5E9, volt*3.4E9-3.9E9))
	freq.append(value)

fig, ax = plt.subplots()
ax.plot(volts, freq)

ax.set(xlabel='Voltage', ylabel='Frequency',
       title='VCO Response')
ax.grid()

fig.savefig("test.png")
plt.show()