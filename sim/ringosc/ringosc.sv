`timescale 1s/1fs

module ringosc (
    input real vdd,
    output var logic out
);
	real freq;
	always begin
		freq = 3.4E9*vdd - 3.9E9;
		if (freq<0.5E9) freq = 0.5E9;
		if (freq>2E9) freq = 2E9;
		
		out = 1;
		#(0.5/freq*1s);
		out = 0;
		#(0.5/freq*1s);
	end



endmodule
