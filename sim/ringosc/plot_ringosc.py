import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
volts = [1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8]
freq_sim = [2.62E8, 5.05E8, 7.87E8, 1.1E9, 1.41E9, 1.73E9, 2.04E9, 2.34E9, 2.63E9]
freq_extract = [1.43E8, 2.79E8, 4.42E8, 6.22E8, 8.13E8, 1.01E9, 1.19E9, 1.38E9, 1.56E9]
freq_ideal = []

volts_ = np.arange(1.0, 1.8, 0.01)
freq_ideal = []
for volt in volts_:
	value = min(2E9, max(0.5E9, volt*3.4E9-3.9E9))
	freq_ideal.append(value)


fig, ax = plt.subplots()
#ax.plot(volts_, freq_ideal, label="ideal")
ax.plot(volts, freq_sim, label="pre layout")
ax.plot(volts, freq_extract, label="post layout")
ax.set(xlabel='Voltage', ylabel='Frequency',
       title='VCO Response')
ax.grid()
plt.legend()

fig.savefig("test1.png", dpi=300)
plt.show()