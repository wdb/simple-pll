`timescale 1s/1fs

module filter #(
    parameter real tau=50e-9
) (
    input real in
);
    // ADD VARIABLE DECLARATIONS HERE AS NECESSARY
    real last_time = 0;
    real out_val = 0;
    real delta_t;
    real in_last;
    function real out();
    	delta_t = $realtime - last_time;
    	last_time = $realtime;
    	out_val = $exp(-delta_t/tau)*out_val + (1 - $exp(-delta_t/tau)) * in;
    	return out_val;

    endfunction

    always @(in) begin
        delta_t = $realtime - last_time;
    	last_time = $realtime;
    	out_val = $exp(-delta_t/tau)*out_val + (1 - $exp(-delta_t/tau)) * in_last;
    	in_last = in;
    end
endmodule
